# Load DCAT Data to a Databus

https://www.w3.org/TR/vocab-dcat-3/

http://dev.dbpedia.org/Databus

## Procedure

1. ✅ Query all datasets from https://datasets.coypu.org/
2. ✅ get the metadata for each dataset
3. ✅ get the fileset from the metadata
4. ✅ download the file
5. ✅ build an artifact url (e.g. https://databus.coypu.org/narndt/coypu/countries)
6. ✅ upload the file to the databus webdav (e.g. 2023-09-18T122214Z-static-countries.ttl)
7. ✅ take the metadata and convert it to the correct databus metadata
8. ✅ upload the metadata

## TODO
- Report download errors
- Add an option to register the group on the databus
- databus adapter Move artifact metadata from version to artifact
- correctly identify newly created version of daily, weekyl and monthly datasets

```
# version           <- CoyPuMtDistribution.version
# content_variants  <- CoyPuMtDistribution.content_variants
# fileformat        <- CoyPuMtDistribution.fileformat
# license_iri       <- CoyPuMtDataset.license
# source_dataset    <- CoyPuMtDataset.source
# title             <- CoyPuMtDataset.label
# abstract          <- CoyPuMtDataset.description
# description       <- CoyPuMtDataset.description
```

# Example:

Group Metadata

```
[
  {
    "@id": "https://databus.coypu.org/narndt/coypu",
    "@type": "Group",
    "title": "CoyPu"
  }
]
```

Artifact Metadata

```
[
  {
    "@id": "https://databus.coypu.org/narndt/coypu/countries",
    "@type": "Artifact",
    "title": "Countries",
    "abstract": "Counties and regions",
    "description": "Counties and regions"
  }
]
```


Version Metadata

```
[
  {
    "@type": [
      "Version",
      "Dataset"
    ],
    "@id": "https://databus.coypu.org/narndt/coypu/countries/2023-09-18T12",
    "hasVersion": "2023-09-18T12",
    "title": "Countries",
    "abstract": "Countries 2023-09-18T12:22:14Z",
    "description": "Countries 2023-09-18T12:22:14Z",
    "license": "https://dalicc.net/licenselibrary/Cc010Universal",
    "wasDerivedFrom": "https://metadata.coypu.org/dataset/wikidata-distribution",
    "distribution": [
      {
        "@type": "Part",
        "formatExtension": "ttl",
        "compression": "none",
        "downloadURL": "https://databus.coypu.org/dav/narndt/coypu/countries/2023-09-18T122214Z/countries_freqency=static.ttl",
        "dcv:frequency": "static"
      }
    ]
  }
]
```

Full Publish Request Body

```
{
  "@context": "https://databus.coypu.org/res/context.jsonld",
  "@graph": [
    {
      "@id": "https://databus.coypu.org/narndt/coypu",
      "@type": "Group",
      "title": "CoyPu"
    },
    {
      "@id": "https://databus.coypu.org/narndt/coypu/countries",
      "@type": "Artifact",
      "title": "Countries",
      "abstract": "Counties and regions",
      "description": "Counties and regions"
    },
    {
      "@type": [
        "Version",
        "Dataset"
      ],
      "@id": "https://databus.coypu.org/narndt/coypu/countries/2023-09-18T12",
      "hasVersion": "2023-09-18T12",
      "title": "Countries",
      "abstract": "Countries 2023-09-18T12:22:14Z",
      "description": "Countries 2023-09-18T12:22:14Z",
      "license": "https://dalicc.net/licenselibrary/Cc010Universal",
      "wasDerivedFrom": "https://metadata.coypu.org/dataset/wikidata-distribution",
      "distribution": [
        {
          "@type": "Part",
          "formatExtension": "ttl",
          "compression": "none",
          "downloadURL": "https://databus.coypu.org/dav/narndt/coypu/countries/2023-09-18T122214Z/countries_freqency=static.ttl",
          "dcv:frequency": "static"
        }
      ]
    }
  ]
}
```

Query datasets, named graphs and distributions from the coypu dataset catalog:

```
prefix mt: <https://schema.coypu.org/metadata-template#>
prefix dct: <http://purl.org/dc/terms/>
prefix dcat: <http://www.w3.org/ns/dcat#>
prefix prov: <http://www.w3.org/ns/prov#>

construct {
  ?Dataset a mt:Dataset ;
  	mt:graph ?Named_Graph ;
    mt:theme ?theme ;
    dct:identifier ?label ;
    dct:license ?license ;
    mt:licensePage ?license ;
    prov:wasDerivedFrom ?source .

  ?Named_Graph a mt:DatasetGraph ;
    dct:date ?date ;
   	mt:fileSet ?fileSet .

  ?fileSet a dcat:Distribution ;
   	dct:modified ?modified ;
    mt:fileName ?fileName ;
    dcat:downloadURL ?downloadUrl ;
    dcat:byteSize ?byteSize ;
    mt:frequency ?frequency .

} where {
  ?Dataset mt:graph ?Named_Graph .
  ?Named_Graph a mt:DatasetGraph ;
   	mt:fileSet ?fileSet .
  ?fileSet dcat:downloadURL ?downloadUrl .

  optional { ?Dataset mt:theme ?theme }
  optional { ?Dataset dct:identifier ?label }
  optional { ?Dataset dct:license ?license }
  optional { ?Dataset mt:licensePage ?license }
  optional { ?Dataset dcat:distribution/dcat:downloadURL ?source }

  optional { ?Named_Graph dct:date ?date }

  optional { ?fileSet dct:modified ?modified }
  optional { ?fileSet mt:fileName ?fileName }
  optional { ?fileSet dcat:byteSize ?byteSize }
  optional { ?fileSet mt:frequency ?frequency }
}
```
