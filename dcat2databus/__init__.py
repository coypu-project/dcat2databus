import click
from os import getenv
from dotenv import load_dotenv
from pathlib import Path
from rdflib.term import URIRef

from .coypumt_adapter import CoyPuMtAdapter
from .databus_adapter import DatabusAdapter
from .databus_webdav_adapter import DatabusWebDAVAdapter
from .file_loader import FileLoader
from .utils import user_pw_to_auth


@click.group()
@click.option("--debug/--no-debug", default=False)
@click.option("--env", default="default.env", help="The environment file to read")
def cli(debug, env):
    dotenv_path = Path(env)
    load_dotenv(dotenv_path=dotenv_path)
    click.echo(f"Debug mode is {'on' if debug else 'off'}")


@cli.command()
@click.option(
    "coypu_endpoint",
    "-e",
    "--endpoint",
    default=None,
    help="The SPARQL endpoint onf the CoyPu MT/DCAT catalog",
)
@click.option(
    "coypu_username",
    "-u",
    "--username",
    default=None,
    help="If authentication is required for the CoyPu MT/DCAT catalog the username to be used for authentication",
)
@click.option(
    "coypu_password",
    "-p",
    "--password",
    default=None,
    help="If authentication is required for the CoyPu MT/DCAT catalog the password to be used for authentication, if a username is given, but no password, it will be prompted for the password",
)
def list(coypu_endpoint, coypu_username, coypu_password):
    coypu_endpoint = coypu_endpoint or getenv("COYPU_QUERY_ENDPOINT")
    coypu_username = coypu_username or getenv("COYPU_LOGIN_USERNAME")
    coypu_password = coypu_password or getenv("COYPU_LOGIN_PASSWORD")

    dcat = CoyPuMtAdapter(
        coypu_endpoint, user_pw_to_auth(coypu_username, coypu_password)
    )
    dl = dcat.list_datasets()
    for d in dl:
        print(f"{d.iri.n3()}: {d.description}")
        print(f"{d.iri.n3()}: {d.label}")
        print(f"{d.iri.n3()}: {d.license}")
        print(f"{d.iri.n3()}: {d.source}")
        if d.iri != URIRef("https://metadata.coypu.org/dataset/wikievents"):
            for ng in d.namedgraphs:
                for fs in ng.filesets:
                    print(f"{d.iri.n3()}: {ng.iri.n3()}: {fs.iri.n3()}: {fs.version}")
                    print(
                        f"{d.iri.n3()}: {ng.iri.n3()}: {fs.iri.n3()}: {fs.content_variants}"
                    )
                    print(
                        f"{d.iri.n3()}: {ng.iri.n3()}: {fs.iri.n3()}: {fs.fileformat}"
                    )
                    print(f"{d.iri.n3()}: {ng.iri.n3()}: {fs.iri.n3()}: {fs.byte_size}")


@cli.command()
@click.option(
    "dataset_iri",
    "-d",
    "--dataset-iri",
    "--dataset",
    default=None,
    help="The Dataset IRI on the CoyPu Dataset Catalog",
)
@click.option(
    "coypu_endpoint",
    "-e",
    "--endpoint",
    default=None,
    help="The SPARQL endpoint onf the CoyPu MT/DCAT catalog",
)
@click.option(
    "coypu_username",
    "-u",
    "--username",
    default=None,
    help="If authentication is required for the CoyPu MT/DCAT catalog the username to be used for authentication",
)
@click.option(
    "coypu_password",
    "-p",
    "--password",
    default=None,
    help="If authentication is required for the CoyPu MT/DCAT catalog the password to be used for authentication, if a username is given, but no password, it will be prompted for the password",
)
@click.option(
    "databus_baseuri", "--databus-baseuri", default=None, help="The databus base uri"
)
@click.option(
    "databus_publisher",
    "--publisher",
    default=None,
    help="The databus publisher name (basically your username)",
)
@click.option(
    "databus_apikey", "--apikey", default=None, help="The API key on the databus"
)
@click.option(
    "fileloader_directory", "--directory", default="data/", help="The directory to store the downloaded datasets in."
)
def transfer(
    dataset_iri,
    coypu_endpoint,
    coypu_username,
    coypu_password,
    databus_baseuri,
    databus_publisher,
    databus_apikey,
    fileloader_directory,
):
    coypu_endpoint = coypu_endpoint or getenv("COYPU_QUERY_ENDPOINT")
    coypu_username = coypu_username or getenv("COYPU_LOGIN_USERNAME")
    coypu_password = coypu_password or getenv("COYPU_LOGIN_PASSWORD")
    databus_baseuri = databus_baseuri or getenv("DATABUS_BASE_URI")
    databus_publisher = databus_publisher or getenv("DATABUS_PUBLISHER")
    databus_apikey = databus_apikey or getenv("DATABUS_API_KEY")
    fileloader_directory = fileloader_directory or getenv("FILELOADER_DIRECTORY")

    coypu_mt = CoyPuMtAdapter(
        coypu_endpoint, user_pw_to_auth(coypu_username, coypu_password)
    )
    dwl = FileLoader(fileloader_directory, user_pw_to_auth(coypu_username, coypu_password))
    bus = DatabusAdapter(
        databus_baseuri, user_pw_to_auth(databus_publisher, databus_apikey)
    )
    bus_webdav = DatabusWebDAVAdapter(
        bus, user_pw_to_auth(databus_publisher, databus_apikey)
    )

    dataset = coypu_mt.dataset(URIRef(dataset_iri))

    print(f"{dataset.iri.n3()}:")
    print(f"{dataset.iri.n3()}: {dataset.description}")
    print(f"{dataset.iri.n3()}: {dataset.label}")
    print(f"{dataset.iri.n3()}: {dataset.license}")
    print(f"{dataset.iri.n3()}: {dataset.source}")

    for namedgraph in dataset.namedgraphs:
        artifact = bus.generate_artifact("coypu", namedgraph.artifact_name)
        print(artifact.uri)

        for fileset in namedgraph.filesets:
            print(
                f"{dataset.iri.n3()}: {namedgraph.iri.n3()}: {fileset.iri.n3()}: {fileset.version}"
            )
            print(
                f"{dataset.iri.n3()}: {namedgraph.iri.n3()}: {fileset.iri.n3()}: {fileset.content_variants}"
            )
            print(
                f"{dataset.iri.n3()}: {namedgraph.iri.n3()}: {fileset.iri.n3()}: {fileset.fileformat}"
            )
            print(
                f"{dataset.iri.n3()}: {namedgraph.iri.n3()}: {fileset.iri.n3()}: {fileset.byte_size}"
            )
            print(
                f"{dataset.iri.n3()}: {namedgraph.iri.n3()}: {fileset.iri.n3()}: {fileset.download_url}"
            )

            version = artifact.generate_version(
                fileset.version[0],
                fileset.content_variants,
                fileset.fileformat[0],
                fileset.compression[0],
                dataset.license[0] if len(dataset.license) > 0 else URIRef("https://example.org/no-license-known"),
                dataset.source[0] if len(dataset.source) > 0 else None,
                dataset.label[0] if len(dataset.label) > 0 else None,
                " ".join(dataset.description),
                " ".join(dataset.description),
            )
            version_webdav = bus_webdav.generate_version(version)

            target_file = dwl.download_fileset(fileset.download_url[0])
            print(version.uri)
            print(version_webdav.file_target_path)

            file_webdav = version_webdav.upload_file(target_file)
            print(file_webdav.sha256_length_tuple)
            print(file_webdav.url)

            version.register(file_webdav)


if __name__ == "__main__":
    cli()
