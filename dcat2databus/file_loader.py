from typing import Optional, Tuple
from pathlib import Path
from os import path
from urllib.parse import urlparse
import requests
from requests.auth import HTTPBasicAuth
from rdflib.term import Node


class FileLoader:
    """This is an interface to get data and information from a dcat catalog."""

    def __init__(self, target_directory: str, auth: Optional[Tuple[str, str]] = None):
        self.target_directory = Path(target_directory)
        self.auth = HTTPBasicAuth(auth[0], auth[1]) if auth else None

    def ensure_target_directory(self):
        self.target_directory.mkdir(parents=True, exist_ok=True)

    def download_fileset(self, fileset: Node) -> str:
        self.ensure_target_directory()

        download_url = str(fileset)
        filename = path.basename(urlparse(download_url).path)
        target_file = path.join(self.target_directory, filename)
        r = requests.get(download_url, auth=self.auth)
        f = open(target_file, "wb")
        f.write(r.content)
        f.close()

        return target_file
