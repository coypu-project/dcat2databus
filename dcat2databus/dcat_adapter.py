from typing import Optional, Tuple
from rdflib.term import Node, URIRef
from rdflib.plugins.stores.sparqlstore import SPARQLStore


class DcatAdapter:
    """This is an interface to get data and information from a dcat catalog."""

    def __init__(self, sparql_endpoint: str, auth: Optional[Tuple[str, str]] = None):
        self.store = SPARQLStore(sparql_endpoint, auth=auth)

    def list_datasets(self) -> list[URIRef]:
        dataset_query = """
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select distinct ?Dataset {
          ?Dataset a dcat:Dataset
        }
        """
        result = self.store.query(dataset_query)
        return [row["Dataset"] for row in result if isinstance(row["Dataset"], URIRef)]

    def get_distribution_for_dataset(self, dataset: URIRef) -> list[Node]:
        namedgraph_query = f"""
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select distinct ?distribution {{
          {dataset.n3()} dcat:distribution ?distribution
        }}
        """
        result = self.store.query(namedgraph_query)
        return [row["NamedGraph"] for row in result]

    def get_downloadurl_for_distribution(self, distribution: URIRef) -> list[Node]:
        fileset_query = f"""
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select ?downloadUrl {{
          {distribution.n3()} dcat:downloadURL ?downloadUrl
        }}
        """
        result = self.store.query(fileset_query)
        return [row["downloadUrl"] for row in result]
