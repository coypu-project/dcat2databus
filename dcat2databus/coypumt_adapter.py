from __future__ import (
    annotations,
)  # relevant to get the circular dependency between typehints for CoyPuMtAdapter and CoyPuMtArtifactVersion working

from typing import Optional, Tuple
from rdflib.term import Node, URIRef, Literal
from rdflib.plugins.stores.sparqlstore import SPARQLStore

from os.path import splitext
from urllib.parse import urlparse


class CoyPuMtAdapter:
    """This is an interface to get data and information from the coypu dataset catalog."""

    def __init__(self, sparql_endpoint: str, auth: Optional[Tuple[str, str]] = None):
        self.store = SPARQLStore(sparql_endpoint, auth=auth)

    def list_datasets(self) -> list[URIRef]:
        query = """
        prefix mt:     <https://schema.coypu.org/metadata-template#>

        select distinct ?Dataset {
          ?Dataset mt:graph ?Named_Graph .
          ?Named_Graph a mt:DatasetGraph .
        }
        """
        result = self.store.query(query)
        return [
            CoyPuMtDataset(self, row["Dataset"])
            for row in result
            if isinstance(row["Dataset"], URIRef)
        ]

    def dataset(self, dataset: str) -> CoyPuMtDataset:
        """
        The amount of data that should correspond with a dataset on the databus, drawn from the coypu dataset catalog identified with the dataset iri.
        """
        return CoyPuMtDataset(self, dataset)


class CoyPuMtDataset:
    """
    This class should represent the amount of data from the coypu dataset catalog that correspond to a dataset. This corresponds to something that represents a source of an Artifact on the databus or a group, we will see.
    The data is collected from the coypu dataset catalog using terms from the DCAT and CoyPu Metadata Terms Vocabularies.
    """

    def __init__(self, adapter: CoyPuMtAdapter, iri: URIRef):
        self.adapter = adapter
        self.iri = iri

    @property
    def namedgraphs(self) -> list[Node]:
        query = f"""
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select distinct ?Named_Graph {{
          {self.iri.n3()} mt:graph ?Named_Graph .
          ?Named_Graph a mt:DatasetGraph .
        }}
        """
        result = self.adapter.store.query(query)
        return [CoyPuMtNamedGraph(self, row["Named_Graph"]) for row in result]

    @property
    def description(self) -> list[Literal]:
        query = f"""
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select distinct ?theme {{
          {self.iri.n3()} mt:theme ?theme .
        }}
        """
        result = self.adapter.store.query(query)
        return [row["theme"] for row in result]

    @property
    def label(self) -> list[Literal]:
        query = f"""
        prefix dct:   <http://purl.org/dc/terms/>

        select distinct ?label {{
          {self.iri.n3()} dct:identifier ?label .
        }}
        """
        result = self.adapter.store.query(query)
        return [row["label"] for row in result]

    @property
    def license(self) -> list[Node]:
        """The license is inherited from the source dataset."""
        query = f"""
        prefix dct: <http://purl.org/dc/terms/>
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select distinct ?license {{
          {{
            {self.iri.n3()} dct:license ?license .
          }} union {{
            {self.iri.n3()} mt:licensePage ?license .
          }}
        }}
        """
        result = self.adapter.store.query(query)
        return [row["license"] for row in result]

    @property
    def source(self) -> list[Node]:
        """The source of the dataset."""
        query = f"""
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select distinct ?downloadURL {{
          {self.iri.n3()} dcat:distribution/dcat:downloadURL ?downloadURL .
        }}
        """
        result = self.adapter.store.query(query)
        return [row["downloadURL"] for row in result]
        pass


class CoyPuMtNamedGraph:
    """
    This class should represent the named graph on the coypu dataset catalog. This corresponds to an Artifact on the databus.
    The data is collected from the coypu dataset catalog using terms from the DCAT and CoyPu Metadata Terms Vocabularies.
    """

    def __init__(self, dataset: CoyPuMtDataset, iri: URIRef):
        self.adapter = dataset.adapter
        self.dataset = dataset
        self.iri = iri

    @property
    def artifact_name(self) -> str:
        return "_".join(filter(None, urlparse(self.iri).path.split("/")))

    @property
    def date(self) -> list[Node]:
        """The date of the named graph."""
        query = f"""
        prefix dct: <http://purl.org/dc/terms/>

        select distinct ?date {{
          {self.iri.n3()} dct:date ?date .
        }}
        """
        result = self.adapter.store.query(query)
        return [row["date"] for row in result]

    @property
    def filesets(self) -> list[Node]:
        query = f"""
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select ?fileSet {{
          {self.iri.n3()} mt:fileSet ?fileSet
        }}
        """
        result = self.adapter.store.query(query)
        return [CoyPuMtDistribution(self, row["fileSet"]) for row in result]

    @property
    def get_distribution(self, distribution) -> CoyPuMtDistribution:
        return [CoyPuMtDistribution(self, distribution)]


class CoyPuMtDistribution:
    """
    This class should represent the amount of data from the coypu dataset catalog that correspond to a distribution. This corresponds to an Artifact's Version on the databus.
    The data is collected from the coypu dataset catalog using terms from the DCAT and CoyPu Metadata Terms Vocabularies.
    """

    def __init__(self, namedgraph: CoyPuMtNamedGraph, iri: URIRef):
        self.adapter = namedgraph.adapter
        self.namedgraph = namedgraph
        self.iri = iri

    @property
    def modified(self) -> list[Node]:
        """The modification date of the distribution.

        Fallback could be CoyPuMtNamedGraph.date
        """
        query = f"""
        prefix dct: <http://purl.org/dc/terms/>

        select distinct ?modified {{
          {self.iri.n3()} dct:modified ?modified .
        }}
        """
        result = self.adapter.store.query(query)
        return [row["modified"].replace("+", "_").replace(":", "-") for row in result]

    @property
    def version(self) -> list[Node]:
        """The version is taken from the modification date of the distribution."""
        return self.modified

    @property
    def filename(self) -> list[Node]:
        query = f"""
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select ?fileName {{
          {self.iri.n3()} mt:fileName ?fileName
        }}
        """
        result = self.adapter.store.query(query)
        return [row["fileName"] for row in result]

    @property
    def fileformat(self) -> list[Node]:
        """Return only the fileextension"""
        def get_file_extension(filename):
            basename, extension = splitext(filename)
            if splitext(basename)[1]:
                basename, extensionL2 = splitext(basename)
                extension = extensionL2 + extension
            if extension and extension[1:]:
                return extension[1:]
            return None
        return [get_file_extension(row) for row in self.filename]

    @property
    def compression(self) -> list[Node]:
        """Return only the compression"""
        def get_compression(filename):
            basename, extension = splitext(filename)
            if extension[1:] in ["bz2", "gz", "lz", "7z", "rar", "zip", "xz"]:
                return extension[1:]
            return None
        return [get_compression(row) for row in self.filename]

    @property
    def download_url(self) -> list[Node]:
        query = f"""
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select ?downloadUrl {{
          {self.iri.n3()} dcat:downloadURL ?downloadUrl
        }}
        """
        result = self.adapter.store.query(query)
        return [row["downloadUrl"] for row in result]

    @property
    def byte_size(self) -> list[Node]:
        query = f"""
        prefix dcat: <http://www.w3.org/ns/dcat#>

        select ?byteSize {{
          {self.iri.n3()} dcat:byteSize ?byteSize
        }}
        """
        result = self.adapter.store.query(query)
        return [row["byteSize"] for row in result]

    @property
    def content_variants(self) -> list[Node]:
        query = f"""
        prefix mt: <https://schema.coypu.org/metadata-template#>

        select ?frequency {{
          {self.iri.n3()} mt:frequency ?frequency
        }}
        """
        result = self.adapter.store.query(query)
        return {"frequency": row["frequency"] for row in result}
