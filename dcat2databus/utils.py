from typing import Tuple, List, Optional
from click import prompt
from posixpath import join
import requests


def user_pw_to_auth(
    username: Optional[str] = None, password: Optional[str] = None
) -> Tuple[str, str]:
    if username is not None:
        if password is None:
            password = prompt("Please enter the password", type=str, hide_input=True)
        return (username, password)
    return None


class WebDAVHandler:
    """Work with a WebDAV endpoint."""

    def __init__(self, dav_base: str, api_key: str):
        self.dav_base = dav_base
        self.api_key = api_key

    def check_existence(self, path: str) -> bool:
        """check if path is available"""
        try:
            resp = requests.head(url=join(self.dav_base, path), timeout=4)
        except requests.RequestException:
            return False

        return bool(resp.status_code == 405)

    def create_dir(
        self, path: str, session: Optional[requests.Session] = None
    ) -> requests.Response:
        """create directory"""

        if session is None:
            session = requests.Session()

        req = requests.Request(
            method="MKCOL",
            url=join(self.dav_base, path),
            headers={"X-API-KEY": f"{self.api_key}"},
        )
        resp = session.send(req.prepare())
        return resp

    def create_dirs(self, path: str) -> List[requests.Response]:
        """create directories"""

        dirs = path.split("/")
        responses = []
        current_path = ""
        for directory in dirs:
            current_path = current_path + directory + "/"
            if not self.check_existence(current_path):
                resp = self.create_dir(current_path)
                responses.append(resp)
                if resp.status_code not in [200, 201, 405]:
                    raise WebDAVException(resp)

        return responses

    def upload_file(
        self, path: str, data: bytes, create_parent_dirs: bool = False
    ) -> requests.Response:
        """upload data in bytes to a path, optionally creating parent dirs."""

        if create_parent_dirs:
            dirpath = path.rsplit("/", 1)[0]
            self.create_dirs(dirpath)

        resp = requests.put(
            url=join(self.dav_base, path),
            headers={"X-API-KEY": f"{self.api_key}"},
            data=data,
            timeout=3000,
        )

        return resp


class WebDAVException(Exception):
    """Generalized exception for WebDAV requests"""

    def __init__(self, resp: requests.Response):
        super().__init__(
            f"Exception during WebDAV Request {resp.request.method} to "
            f"{resp.request.url}: Status {resp.status_code}\nResponse: {resp.text}"
        )
