from __future__ import (
    annotations,
)  # relevant to get the circular dependency between typehints for DatabusWebDAVAdapter, DatabusWebDAVVersion, and DatabusWebDAVFile working

from typing import Tuple
from posixpath import join
from hashlib import sha256

from .databus_adapter import DatabusAdapter, DatabusVersion, DatabusFile
from .utils import WebDAVHandler, WebDAVException


class DatabusWebDAVAdapter:
    """This is an interface to get data and information from the coypu dataset catalog."""

    def __init__(self, databus_adapter: DatabusAdapter, auth: Tuple[str, str]):
        """The auth tuple consists of publisher and API Key"""
        self.databus_adapter = databus_adapter
        self.publisher = auth[0]
        self.api_key = auth[1]
        self.webdav_base = join(self.databus_adapter.baseuri, "dav", self.publisher)

        self.webdav_handler = WebDAVHandler(self.webdav_base, self.api_key)

    def generate_version(self, version: DatabusVersion) -> DatabusWebDAVVersion:
        """
        The File of the Version oo the Databus WebDAV.
        """
        return DatabusWebDAVVersion(self, version)


class DatabusWebDAVVersion:
    def __init__(self, adapter: DatabusWebDAVAdapter, version: DatabusVersion):
        self.adapter = adapter
        self.version = version

    @property
    def content_variants_string(self) -> str:
        return "_".join([f"{k}={v}" for k, v in self.version.content_variants.items()])

    @property
    def file_name(self) -> str:
        return f"{self.version.artifact.artifact}_{self.content_variants_string}.{self.version.fileformat}"

    @property
    def file_target_path(self) -> str:
        return join(
            self.version.artifact.group,
            self.version.artifact.artifact,
            self.version.version,
            self.file_name,
        )

    def upload_file(self, file_path: str) -> DatabusWebDAVFile:
        """Open a file, read it and upload it to the webdav.
        return the the legth in bytes
        """
        with open(file_path, "rb") as in_file:
            return self.upload_bytes(in_file.read())

    def upload_bytes(self, data: bytes) -> DatabusWebDAVFile:
        """Upload an artifacts version to the databus webdav.
        return the the legth in bytes
        """

        sha256sum = sha256(bytes(data)).hexdigest()
        content_length = len(data)

        upload_resp = self.adapter.webdav_handler.upload_file(
            path=self.file_target_path,
            data=bytes(data),
            create_parent_dirs=True,
        )
        if upload_resp.status_code >= 400:
            raise WebDAVException(upload_resp)

        return DatabusWebDAVFile(self, sha256sum, content_length)


class DatabusWebDAVFile(DatabusFile):
    def __init__(
        self, version: DatabusWebDAVVersion, sha256sum: str, content_length: int
    ):
        url = join(version.adapter.webdav_handler.dav_base, version.file_target_path)
        DatabusFile.__init__(self, url, sha256sum, content_length)
