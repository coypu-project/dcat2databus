from __future__ import (
    annotations,
)  # relevant to get the circular dependency between typehints for DatabusAdapter and DatabusArtifact working

import json
from typing import Tuple, Dict
from rdflib.term import URIRef
from urllib.parse import urljoin
from posixpath import join
from databusclient import create_distribution, create_dataset, deploy


class DatabusAdapter:
    """This is an interface to register data at a databus."""

    def __init__(self, databus_baseuri: str, auth: Tuple[str, str]):
        """The auth tuple consists of publisher and API Key"""
        self.databus_baseuri = databus_baseuri
        self.baseuri = databus_baseuri
        self.publisher = auth[0]
        self.api_key = auth[1]

    def generate_artifact(self, group: str, artifact: str) -> DatabusArtifact:
        """
        The Databus Dataset Artifact for this specific dataset. It conforms to following conventions: https://{DATABUS_BASE_URI}/{PUBLISHER}/{GROUP}/{ARTIFACT}/
        """
        return DatabusArtifact(self, group, artifact)


class DatabusArtifact:
    """The representation of an artifact on the databus."""

    def __init__(self, adapter: DatabusAdapter, group: str, artifact: str):
        self.adapter = adapter
        self.group = group
        self.artifact = artifact

    @property
    def uri(self) -> URIRef:
        return URIRef(
            urljoin(
                self.adapter.databus_baseuri,
                join(self.adapter.publisher, self.group, self.artifact),
            )
        )

    def generate_version(
        self,
        version: str,
        content_variants: Dict[str, str],
        fileformat: str,
        compression: str,
        license_iri: URIRef,
        source_dataset: str,
        title: str,
        abstract: str,
        description: str,
    ) -> DatabusVersion:
        """
        The Version of the Databus Dataset Artifact.
        """
        return DatabusVersion(
            self,
            version,
            content_variants,
            fileformat,
            compression,
            license_iri,
            source_dataset,
            title,
            abstract,
            description,
        )


class DatabusVersion:
    """The representation of an artifacts version on the databus."""

    def __init__(
        self,
        artifact: DatabusArtifact,
        version: str,
        content_variants: Dict[str, str],
        fileformat: str,
        compression: str,
        license_iri: URIRef,
        source_dataset: str,
        title: str,
        abstract: str,
        description: str,
    ):
        self.adapter = artifact.adapter
        self.artifact = artifact
        self.version = version
        self.content_variants = content_variants
        self.fileformat = fileformat
        self.compression = compression
        self.license_iri = license_iri
        self.source_dataset = source_dataset
        self.title = title
        self.abstract = abstract
        self.description = description
        self._sha256_length_tuple = None

    @property
    def uri(self) -> URIRef:
        return URIRef(
            urljoin(
                self.adapter.databus_baseuri,
                join(
                    self.adapter.publisher,
                    self.artifact.group,
                    self.artifact.artifact,
                    self.version,
                ),
            )
        )

    def register(self, file: DatabusFile):
        distribution_string = create_distribution(
            url=file.url,
            cvs=self.content_variants,
            file_format=self.fileformat,
            compression=self.compression,
            sha256_length_tuple=file.sha256_length_tuple,
        )
        print(f"Distrib String: {distribution_string}")
        dataset = create_dataset(
            version_id=self.uri,
            title=self.title,
            abstract=self.abstract,
            description=self.description,
            license_url=self.license_iri,
            distributions=[distribution_string],
            derived_from=self.source_dataset,
        )
        print(f"Submit Dataset to Databus: {json.dumps(dataset, indent=2)}")
        deploy(dataset, self.adapter.api_key)


class DatabusFile:
    def __init__(self, url: str, sha256sum: str, content_length: int):
        self.url = url
        self.sha256sum = sha256sum
        self.content_length = content_length

    @property
    def sha256_length_tuple(self) -> Tuple[str, int]:
        return (self.sha256sum, self.content_length)
